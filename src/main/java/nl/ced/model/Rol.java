/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.ced.model;

/**
 *
 * @author Reinier
 */
public class Rol {
    String naam;
    private int id;
    
    public Rol() {
        
    }

    /**
     * Instantiates a new Rol.
     *
     * @param id role ID
     * @param naam role name
     */
    public Rol(int id, String naam) {
        super();
        this.id = id;
        this.naam = naam;
    }

    public int getId() {
        return id;
    }

    public String getNaam() {
        return naam;
    }
}
