package nl.ced.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import java.security.Principal;
import nl.ced.View;

/**
 *
 * @author dylba
 */
public class Afdeling implements Principal{
    @JsonView(View.Public.class)
    private int id;
    
    @JsonView(View.Public.class)
    private int type;
    
    @JsonView(View.Public.class)
    private String property_adres;
    
    @JsonView(View.Public.class)
    private String property_plaats;
    
    @JsonView(View.Public.class)
    private String property_huisnr;
    
    @JsonView(View.Public.class)
    private String property_toevoeging;
    
    @JsonView(View.Public.class)
    private String property_type;
    
    @JsonView(View.Public.class)
    private String mobility_merk;
    
    @JsonView(View.Public.class)
    private String mobility_type;
    
    @JsonView(View.Public.class)
    private String mobility_kenteken;
    
    @JsonView(View.Public.class)
    private int mobility_automaat;
    
    @JsonView(View.Public.class)
    private String vitality_slachtoffernaam;
    
    @JsonView(View.Public.class)
    private String vitality_beschrijving;
    
    @JsonView(View.Public.class)
    private String postcode;
    
    @JsonView(View.Public.class)
    private String naam_verzekerde;
    
    @JsonView(View.Public.class)
    private String huisnr;
    
    public Afdeling(){
        
    }
    
    @JsonIgnore
    public Afdeling(int id, String property_adres, 
            String property_plaats, String property_huisnr, 
            String property_toevoeging, String property_type, String postcode, 
            String naam_verzekerde){
        this.id = id;
        this.property_adres = property_adres;
        this.property_plaats = property_plaats;
        this.property_huisnr = property_huisnr;
        this.postcode = postcode;
        this.property_toevoeging = property_toevoeging;
        this.property_type = property_type;
        this.naam_verzekerde = naam_verzekerde;
    }
    @JsonIgnore
    public Afdeling(int id, String mobility_kenteken, 
            int mobility_automaat, String postcode,
            String naam_verzekerde, String huisnr){
        this.id = id;
        this.mobility_kenteken = mobility_kenteken;
        this.mobility_automaat = mobility_automaat;
        this.postcode = postcode;
        this.naam_verzekerde = naam_verzekerde;
        this.huisnr = huisnr;
    }
    
    @JsonIgnore
    public Afdeling(int id, String vitality_slachtoffernaam, 
            String vitality_beschrijving, String postcode,  String huisnr){
        this.id = id;
        this.vitality_slachtoffernaam = vitality_slachtoffernaam;
        this.vitality_beschrijving = vitality_beschrijving;
        this.postcode = postcode;
        this.huisnr = huisnr;
    }
    public String getNaam_verzekerde() {
        return naam_verzekerde;
    }

    public void setNaam_verzekerde(String naam_verzekerde) {
        this.naam_verzekerde = naam_verzekerde;
    }

    public String getHuisnr() {
        return huisnr;
    }

    public void setHuisnr(String huisnr) {
        this.huisnr = huisnr;
    }
    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getProperty_adres() {
        return property_adres;
    }

    public void setProperty_adres(String property_adres) {
        this.property_adres = property_adres;
    }

    public String getProperty_plaats() {
        return property_plaats;
    }

    public void setProperty_plaats(String property_plaats) {
        this.property_plaats = property_plaats;
    }

    public String getProperty_huisnr() {
        return property_huisnr;
    }

    public void setProperty_huisnr(String property_huisnr) {
        this.property_huisnr = property_huisnr;
    }

    public String getProperty_toevoeging() {
        return property_toevoeging;
    }

    public void setProperty_toevoeging(String property_toevoeging) {
        this.property_toevoeging = property_toevoeging;
    }

    public String getProperty_type() {
        return property_type;
    }

    public void setProperty_type(String property_type) {
        this.property_type = property_type;
    }

    public String getMobility_merk() {
        return mobility_merk;
    }

    public void setMobility_merk(String mobility_merk) {
        this.mobility_merk = mobility_merk;
    }

    public String getMobility_type() {
        return mobility_type;
    }

    public void setMobility_type(String mobility_type) {
        this.mobility_type = mobility_type;
    }

    public String getMobility_kenteken() {
        return mobility_kenteken;
    }

    public void setMobility_kenteken(String mobility_kenteken) {
        this.mobility_kenteken = mobility_kenteken;
    }

    public int getMobility_automaat() {
        return mobility_automaat;
    }

    public void setMobility_automaat(int mobility_automaat) {
        this.mobility_automaat = mobility_automaat;
    }

    public String getVitality_slachtoffernaam() {
        return vitality_slachtoffernaam;
    }

    public void setVitality_slachtoffernaam(String vitality_slachtoffernaam) {
        this.vitality_slachtoffernaam = vitality_slachtoffernaam;
    }

    public String getVitality_beschrijving() {
        return vitality_beschrijving;
    }

    public void setVitality_beschrijving(String vitality_beschrijving) {
        this.vitality_beschrijving = vitality_beschrijving;
    }
    
    @Override
    @JsonIgnore
    public String getName() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
