package nl.ced.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import java.security.Principal;
import java.util.ArrayList;
import nl.ced.View;
import nl.ced.persistence.DossierDAO;
import org.hibernate.validator.constraints.NotEmpty;



/**
 *
 * @author Dylan
 */
public class Status {
    @JsonView(View.Public.class)
    private int id;
    
    @JsonView(View.Public.class)
    private String beschrijving;
    
    public Status() {}
    @JsonIgnore
    public Status(String beschrijving, int id){
     this.beschrijving = beschrijving;
     this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBeschrijving() {
        return beschrijving;
    }

    public void setBeschrijving(String beschrijving) {
        this.beschrijving = beschrijving;
    }


    
}
