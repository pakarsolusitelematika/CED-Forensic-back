package nl.ced.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import java.security.Principal;
import java.util.ArrayList;
import nl.ced.View;
import nl.ced.persistence.PersoonDAO;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Meer informatie over validatie:
 *  http://hibernate.org/validator/
 * 
 * @author Peter van Vliet
 */
public class Opdrachtgever implements Principal
{
    @JsonView(View.Public.class)
    private int id;
    
    @JsonView(View.Public.class)
    private String naam;
    
    @JsonView(View.Public.class)
    private String telefoonnummer;
    
    @JsonView(View.Public.class)
    private String contactpersoonnaam;
    
    @JsonView(View.Public.class)
    private String contactpersoon_telefoonnr;
    
    public Opdrachtgever() {}

    @JsonIgnore
    public Opdrachtgever(int id, String naam, String telefoonnummer, 
            String contactpersoon, String contactpersoon_telefoonnr) {
        this.id = id;
        this.naam = naam;
        this.telefoonnummer = telefoonnummer;
        this.contactpersoonnaam = contactpersoon;
        this.contactpersoon_telefoonnr = contactpersoon_telefoonnr;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public String getTelefoonnummer() {
        return telefoonnummer;
    }

    public void setTelefoonnummer(String telefoonnummer) {
        this.telefoonnummer = telefoonnummer;
    }

    public String getContactpersoonnaam() {
        return contactpersoonnaam;
    }

    public void setContactpersoonnaam(String contactpersoonnaam) {
        this.contactpersoonnaam = contactpersoonnaam;
    }

    public String getContactpersoon_telefoonnr() {
        return contactpersoon_telefoonnr;
    }

    public void setContactpersoon_telefoonnr(String contactpersoon_telefoonnr) {
        this.contactpersoon_telefoonnr = contactpersoon_telefoonnr;
    }


    @Override
    @JsonIgnore
    public String getName(){
        return naam;
    }

}
