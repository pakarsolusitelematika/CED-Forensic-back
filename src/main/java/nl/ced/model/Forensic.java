package nl.ced.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import java.security.Principal;
import nl.ced.View;
import nl.ced.persistence.PersoonDAO;

/**
 * @author Dylan Baars
 */
public class Forensic implements Principal
{
    @JsonView(View.Public.class)
    private int id;
    
    @JsonView(View.Public.class)
    private int dossiernummer;
    
    @JsonView(View.Public.class)
    private int onderzoeker_id;
    
    @JsonView(View.Public.class)
    private String factuur_bedrijf;
    
    @JsonView(View.Public.class)
    private String besparing;
    
    
    @JsonView(View.Public.class)
    private Resultaat resultaat;
    
    @JsonView(View.Public.class)
    private String forensic_naam;
    
    public Forensic() {
    }
    @JsonIgnore
    public Forensic(int id, int dossiernummer, int onderzoeker_id, 
            String factuur_bedrijf, String besparing, 
            Resultaat resultaat, String forensic_naam) {
        this.id = id;
        this.dossiernummer = dossiernummer;
        this.onderzoeker_id = onderzoeker_id;
        this.factuur_bedrijf = factuur_bedrijf;
        this.besparing = besparing;
        this.resultaat = resultaat;
        this.forensic_naam = forensic_naam;
    }

    public String getForensic_naam() {
        return forensic_naam;
    }

    public void setForensic_naam(String forensic_naam) {
        this.forensic_naam = forensic_naam;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDossiernummer() {
        return dossiernummer;
    }

    public void setDossiernummer(int dossiernummer) {
        this.dossiernummer = dossiernummer;
    }

    public int getOnderzoeker_id() {
        return onderzoeker_id;
    }

    public void setOnderzoeker_id(int onderzoeker_id) {
        this.onderzoeker_id = onderzoeker_id;
    }

    public String getFactuur_bedrijf() {
        return factuur_bedrijf;
    }

    public void setFactuur_bedrijf(String factuur_bedrijf) {
        this.factuur_bedrijf = factuur_bedrijf;
    }

    public String getBesparing() {
        return besparing;
    }

    public void setBesparing(String besparing) {
        this.besparing = besparing;
    }

    @Override
    @JsonIgnore
    public String getName(){
        return null;
    }

}
