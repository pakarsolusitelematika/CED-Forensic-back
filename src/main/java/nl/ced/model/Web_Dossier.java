package nl.ced.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import java.security.Principal;
import java.util.ArrayList;
import nl.ced.View;
import nl.ced.persistence.DossierDAO;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * 
 * @author Dylan Baar
 */
public class Web_Dossier implements Principal
{
    @JsonView(View.Public.class)
    private int id;
    
    @JsonView(View.Public.class)
    private int nummer;
    
    @JsonView(View.Public.class)
    private int status_id;
    
    @JsonIgnore
    private DossierDAO dao;
    
    @JsonView(View.Public.class)
    private String opdrachtdatum;
    
    @JsonView(View.Public.class)
    private int polisnummer;
    
    @JsonView(View.Public.class)
    private int expert_id;
    
    @JsonView(View.Public.class)
    private int ogever_id;
    
    @JsonView(View.Public.class)
    private String nieuw_registratiedatum;
    
    @JsonView(View.Public.class)
    private String stratiedatum;
    
    @JsonView(View.Public.class)
    private int afdeling_id;
    
    @JsonView(View.Public.class)
    private int kwalificatie;
    
    @JsonView(View.Public.class)
    private int resultaat_id;
    
    @JsonView(View.Public.class)
    private int forensic_id;
    
    @JsonView(View.Public.class)
    private int soortschade_id;
    
    @JsonView(View.Public.class)
    private int schadenummer;
    
    @JsonView(View.Public.class)
    private int afdeling_type;
    
    public Web_Dossier() {}

    @JsonIgnore
    public Web_Dossier(int id, int nummer, int status_id, String opdrachtdatum,
            int polisnummer, int expert_id, int ogever_id, String nieuw_registratiedatum,
            String stratiedatum, int kwalificatie,
            int forensic_id, int soortschade_id, int schadenummer, 
            int afdeling_type, int afdeling_id) {
        this.id = id;
        this.nummer = nummer;
        this.status_id = status_id;
        this.opdrachtdatum = opdrachtdatum;
        this.polisnummer = polisnummer;
        this.expert_id = expert_id;
        this.ogever_id = ogever_id;
        this.nieuw_registratiedatum = nieuw_registratiedatum;
        this.stratiedatum = stratiedatum;
        this.kwalificatie = kwalificatie;
        this.forensic_id = forensic_id;
        this.soortschade_id = soortschade_id;
        this.schadenummer = schadenummer;
        this.afdeling_type = afdeling_type;
        this.afdeling_id = afdeling_id;
    }

    @Override
    @JsonIgnore
    public String getName() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNummer() {
        return nummer;
    }

    public void setNummer(int nummer) {
        this.nummer = nummer;
    }

    public int getStatus_id() {
        return status_id;
    }

    public void setStatus_id(int status_id) {
        this.status_id = status_id;
    }

    public DossierDAO getDao() {
        return dao;
    }

    public void setDao(DossierDAO dao) {
        this.dao = dao;
    }

    public String getOpdrachtdatum() {
        return opdrachtdatum;
    }

    public void setOpdrachtdatum(String opdrachtdatum) {
        this.opdrachtdatum = opdrachtdatum;
    }

    public int getPolisnummer() {
        return polisnummer;
    }
    
    public int getAfdeling_type() {
        return afdeling_type;
    }

    public void setAfdeling_type(int afdeling_type) {
        this.afdeling_type = afdeling_type;
    }
    
    public void setPolisnummer(int polisnummer) {
        this.polisnummer = polisnummer;
    }

    public int getExpert_id() {
        return expert_id;
    }

    public void setExpert_id(int expert_id) {
        this.expert_id = expert_id;
    }

    public int getOgever_id() {
        return ogever_id;
    }

    public void setOgever_id(int ogever_id) {
        this.ogever_id = ogever_id;
    }

    public String getNieuw_registratiedatum() {
        return nieuw_registratiedatum;
    }

    public void setNieuw_registratiedatum(String nieuw_registratiedatum) {
        this.nieuw_registratiedatum = nieuw_registratiedatum;
    }

    public String getStratiedatum() {
        return stratiedatum;
    }

    public void setStratiedatum(String stratiedatum) {
        this.stratiedatum = stratiedatum;
    }

    public int getAfdeling_id() {
        return afdeling_id;
    }

    public void setAfdeling_id(int afdeling_id) {
        this.afdeling_id = afdeling_id;
    }

    public int getKwalificatie() {
        return kwalificatie;
    }

    public void setKwalificatie(int kwalificatie) {
        this.kwalificatie = kwalificatie;
    }

    public int getResultaat_id() {
        return resultaat_id;
    }

    public void setResultaat_id(int resultaat_id) {
        this.resultaat_id = resultaat_id;
    }

    public int getForensic_id() {
        return forensic_id;
    }

    public void setForensic_id(int forensic_id) {
        this.forensic_id = forensic_id;
    }

    public int getSoortschade_id() {
        return soortschade_id;
    }

    public void setSoortschade_id(int soortschade_id) {
        this.soortschade_id = soortschade_id;
    }

    public int getSchadenummer() {
        return schadenummer;
    }

    public void setSchadenummer(int schadenummer) {
        this.schadenummer = schadenummer;
    }

}
