/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.ced.service;

import java.util.Optional;
import io.dropwizard.auth.AuthenticationException;
import io.dropwizard.auth.Authenticator;
import io.dropwizard.auth.Authorizer;
import io.dropwizard.auth.basic.BasicCredentials;
import java.util.logging.Level;
import javax.inject.Inject;
import javax.inject.Singleton;
import nl.ced.model.Persoon;
import nl.ced.persistence.PersoonDAO;
import nl.ced.utility.BCrypt;

/**
 *
 * @author Peter van Vliet
 */
@Singleton
public class AuthenticationService implements Authenticator<BasicCredentials, Persoon>, Authorizer<Persoon>
{
    private final PersoonDAO userDAO;
    
    @Inject
    public AuthenticationService(PersoonDAO userDAO)
    {
        this.userDAO = userDAO;
    }

    @Override
    public Optional<Persoon> authenticate(BasicCredentials credentials) throws AuthenticationException
    {
        Persoon user = userDAO.getByEmail(credentials.getUsername());
        System.out.println(credentials.getPassword());
        System.out.println(user.getWachtwoord());
        System.out.println(BCrypt.checkpw(credentials.getPassword(), user.getWachtwoord()));
        System.out.println("credentials.password bruhh: " + credentials.getPassword() + " " + BCrypt.checkpw(credentials.getPassword(), user.getWachtwoord()) + "user.password: " + user.getWachtwoord());

        
        if (user != null && BCrypt.checkpw(credentials.getPassword(), user.getWachtwoord()))
        {
            return Optional.of(user);
        }
        
        return Optional.empty();
    }

    @Override
    public boolean authorize(Persoon user, String role) {
        return user.hasRole(Integer.parseInt(role));
    }
}
