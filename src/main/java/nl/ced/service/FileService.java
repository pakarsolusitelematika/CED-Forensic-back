package nl.ced.service;

import java.sql.SQLException;
import java.util.Collection;
import javax.inject.Inject;
import javax.inject.Singleton;
import nl.ced.model.Afdeling;
import nl.ced.model.Dossier;
import nl.ced.model.Opdrachtgever;
import nl.ced.model.Status;
import nl.ced.persistence.DossierDAO;





/**
 *
 * @author Peter van Vliet
 */
@Singleton
public class FileService extends BaseService<Dossier> {
    private DossierDAO daoDossier;

    @Inject
    public FileService(DossierDAO daoDossier) {
        this.daoDossier = daoDossier;
    }

    public Collection<Dossier> getAll() {
        return daoDossier.getAll();
    }

    public Collection<Status> getStatus() {
        return daoDossier.getStates();
    }
    public Collection<Opdrachtgever> getClients() {
        return daoDossier.getClients();
    }
    public Dossier get(int id) {
        return daoDossier.get(id);
    }

    public int add(Dossier dossier) {
        return(daoDossier.createDossier(dossier));
    }

    public void createClient(Opdrachtgever opdrachtgever) {
        daoDossier.createClient(opdrachtgever);
    }

    public void archive(int id) {
        daoDossier.archive(id);
    }

    public void recover(int id) {
        daoDossier.recover(id);
    }

    public void setStatus(int id,int status) {
        daoDossier.setStatus(id, status);
    }

    public int addProperty(Afdeling afdeling) {

      return daoDossier.addProperty(afdeling);
    }
    public int addMobility(Afdeling afdeling) {
        return daoDossier.addMobility(afdeling);
    }
    public int addVitality(Afdeling afdeling) {
        return daoDossier.addVitality(afdeling);
    }


 }
