/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.ced.persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;
import javax.inject.Singleton;
import nl.ced.model.Afdeling;
import nl.ced.model.Dossier;
import nl.ced.model.Forensic;
import nl.ced.model.Status;
import nl.ced.model.Persoon;
import nl.ced.model.Opdrachtgever;
import nl.ced.model.Resultaat;
import nl.ced.utility.Database;

/**
 *
 * @author Dylan
 */
@Singleton
public class DossierDAO {
    private Database manager;
    private Connection connection;
    private PersoonDAO persoonDao;
    private PreparedStatement select;
    private PreparedStatement insert;
    private PreparedStatement delete;
    private PreparedStatement check;
    private List<Dossier> dossiers = new ArrayList<>();
    private List<Status> status = new ArrayList<>();
    private List<Opdrachtgever> opdrachtgevers = new ArrayList<>();
    

    public DossierDAO() {
        this.manager = new Database();
        updateDossierList();
        updateStatusList();
    }

    public void updateStatusList() {
        Connection connection = manager.getConnection();        
        status = new ArrayList<>();
        ResultSet resultSet = null;
        try {
            select = connection.prepareStatement(
                    "SELECT status_id, status_beschrijving"
                    + " FROM status;");
            resultSet = select.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            if (resultSet != null) {
                while (resultSet.next()) {
                    status.add(new Status(
                            resultSet.getString("status_beschrijving"),
                            resultSet.getInt("status_id")
                    ));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                select.close();
                connection.close();
            } catch (SQLException e) {
                //
            }
        }
    }
    
    public void updateDossierList() {
        Connection connection = manager.getConnection();
        dossiers = new ArrayList<>();
        
        Afdeling afdeling = null;
        ResultSet resultSet = null;
        try {
            select = connection.prepareStatement(
                    "SELECT dossier_id,dossier_nummer,dossier_status_id,dossier_opdrachtdatum,"
                            + "dossier_polisnummer, dossier_expert_id, dossier_ogever_id, "
                            + "dossier_nieuw_registratiedatum, dossier_stratiedatum,"
                            + "dossier_kwalificatie,dossier_resultaat_id,"
                    + "dossier_forensic_id,dossier_soortschade_id,dossier_schadenummer, "
                    + "afdeling_type, afdeling_id"
                    + " FROM dossier;");
            resultSet = select.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            if (resultSet != null) {
                
                while (resultSet.next()) {
                    Forensic tempFor = null;
                    
                    if (resultSet.getInt("dossier_forensic_id") > 0) {
                        tempFor = getForensic(resultSet.getInt("dossier_forensic_id"), resultSet.getInt("dossier_resultaat_id"));
                    } else {
                        tempFor = new Forensic();
                    }
                    if (resultSet.getInt("afdeling_type") > 0){
                        switch (getAfdelingTypeNaam(resultSet.getInt("afdeling_type"))) {
                            case "vitality":
                                afdeling = getVitality(resultSet.getInt("afdeling_id"));
                                break;
                            case "mobility":
                                afdeling = getMobility(resultSet.getInt("afdeling_id"));
                                System.out.println("I DONT GET IT");
                                System.out.println("I DONT GET IT");
                                System.out.println("I DONT GET IT");
                                System.out.println("I DONT GET IT");
                                System.out.println("I DONT GET IT");
                                System.out.println("I DONT GET IT");
                                System.out.println("I DONT GET IT");
                                System.out.println("I DONT GET IT");
                                System.out.println("I DONT GET IT");
                                
                                break;
                            case "property":
                                afdeling = getProperty(resultSet.getInt("afdeling_id"));
                                break;
                            default:
                                break;
                        }
                    } else {
                        afdeling = new Afdeling();
                    }
                    dossiers.add(new Dossier(
                            resultSet.getInt("dossier_id"),
                            resultSet.getInt("dossier_nummer"),
                            resultSet.getInt("dossier_status_id"),
                            resultSet.getString("dossier_opdrachtdatum"),
                            resultSet.getInt("dossier_polisnummer"),
                            resultSet.getInt("dossier_expert_id"),
                            resultSet.getInt("dossier_ogever_id"),
                            resultSet.getString("dossier_nieuw_registratiedatum"),
                            resultSet.getString("dossier_stratiedatum"),
                            afdeling,
                            resultSet.getInt("dossier_kwalificatie"),
                            resultSet.getInt("dossier_resultaat_id"),
                            resultSet.getInt("dossier_forensic_id"),
                            resultSet.getInt("dossier_soortschade_id"),
                            resultSet.getInt("dossier_schadenummer"),
                            getStatus(resultSet.getInt("dossier_status_id")),
                            getExpert(resultSet.getInt("dossier_expert_id")),
                            getOpdrachtgever(resultSet.getInt("dossier_ogever_id")),
                            tempFor));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                
                select.close();
                connection.close();
            } catch (SQLException e) {
                //
            }
        }
    }
    public String getAfdelingTypeNaam(int afdeling_id) {
        Connection connection = manager.getConnection();
        ResultSet resultSet = null;
        try {
            select = connection.prepareStatement(
                    "SELECT afdeling_type FROM afdeling_type WHERE id = ?");
            select.setInt(1, afdeling_id);
            resultSet = select.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            if (resultSet != null) {
                while (resultSet.next()) {

                    return resultSet.getString("afdeling_type");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                select.close();
                connection.close();
            } catch (SQLException e) {
                //
            }
        }
        return null;
    }

    public int createDossier(Dossier dossier) {
        Connection connection = manager.getConnection();
        int id = 0;
        try {
            insert = connection.prepareStatement("INSERT INTO dossier ("
                    + "dossier_nummer,dossier_status_id,dossier_opdrachtdatum,"
                    + "dossier_polisnummer, dossier_expert_id, dossier_ogever_id,"
                    + "dossier_nieuw_registratiedatum, dossier_stratiedatum,"
                    + "dossier_kwalificatie,dossier_schadenummer,"
                    + "afdeling_type,afdeling_id)"
                    + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?) "
                    + "RETURNING dossier_id;");
            insert.setInt(1, dossier.getNummer());
            insert.setInt(2, dossier.getStatusObjectId());
            insert.setString(3, dossier.getOpdrachtdatum());
            insert.setInt(4, dossier.getPolisnummer());
            insert.setInt(5, dossier.getExpertObjectId());
            insert.setInt(6, dossier.getOpdrachtgeverObjectId());
            insert.setString(7, dossier.getNieuw_registratiedatum());
            insert.setString(8, dossier.getStratiedatum());
            insert.setInt(9, dossier.getKwalificatie());
            insert.setInt(10, dossier.getSchadenummer());
            insert.setInt(11, dossier.getAfdelingObjectType());
            insert.setInt(12, dossier.getAfdelingObjectId());
            ResultSet rs = insert.executeQuery();
            rs.next();
            id = rs.getInt(1);

        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            try {
                insert.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return 0;
            }

            updateDossierList();

            return id;
        }
    }

    public boolean createClient(Opdrachtgever opdrachtgever) {

        //try {
            System.out.println("DossierDAO createClient");
//            insert = connection.prepareStatement("INSERT INTO dossier (dossier_id,dossier_nummer,dossier_status_id,dossier_opdrachtdatum,"
//                    + "dossier_polisnummer, dossier_expert_id, dossier_ogever_id, "
//                    + "dossier_nieuw_registratiedatum, dossier_stratiedatum,"
//                    + "dossier_kwalificatie,dossier_resultaat_id,"
//                    + "dossier_forensic_id,dossier_soortschade_id,dossier_schadenummer)"
//                    + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?);");
//            insert.setInt(1, dossier_id); // dit is misschien iets wat je wilt
//            // veranderen bij het aanmaken van een persoon
//            insert.setInt(2, dossier_nummer);
//            insert.setInt(3, dossier_status_id);
//            insert.setString(4, dossier_opdrachtdatum);



            //insert.executeUpdate();

//        } catch (SQLException e) {
//            e.printStackTrace();
//            return false;
//        } finally {
//            try {
//                insert.close();
//            } catch (SQLException e) {
//                e.printStackTrace();
//                return false;
//            }
//
//            updateDossierList();
//
//            return true;
//        }
        return true;
    }

    public int addProperty(Afdeling afdeling) {
        Connection connection = manager.getConnection();
        int a = 0;
        try {
            insert = connection.prepareStatement("INSERT INTO property "
                    + "(property_adres, property_plaats, "
                    + "property_huisnr, property_toevoeging, property_type, postcode,"
                    + "naam_verzekerde)"
                    + "VALUES (?,?,?,?,?,?,?) "
                    + "RETURNING property_id;");
            insert.setString(1, afdeling.getProperty_adres());
            insert.setString(2, afdeling.getProperty_plaats());
            insert.setString(3, afdeling.getProperty_huisnr());
            insert.setString(4, afdeling.getProperty_toevoeging());
            insert.setString(5, afdeling.getProperty_type());
            insert.setString(6, afdeling.getPostcode());
            insert.setString(7, afdeling.getNaam_verzekerde());
            
            ResultSet rs = insert.executeQuery();
            rs.next();
            a = rs.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            try {
                insert.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return 0;
            }

            updateDossierList();

            return a;
        }
    }

    public int addMobility(Afdeling afdeling) {
        Connection connection = manager.getConnection();
        int a = 0;
        try {
            insert = connection.prepareStatement("INSERT INTO mobility "
                    + "(mobility_kenteken, mobility_automaat, "
                    + "postcode, huisnr, naam_verzekerde)"
                    + "VALUES (?,?,?,?,?) "
                    + "RETURNING mobility_id;");
            insert.setString(1, afdeling.getMobility_kenteken());
            insert.setInt(2, afdeling.getMobility_automaat());
            insert.setString(3, afdeling.getPostcode());
            insert.setString(4, afdeling.getHuisnr());
            insert.setString(5, afdeling.getNaam_verzekerde());
            ResultSet rs = insert.executeQuery();
            rs.next();
            a = rs.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            try {
                insert.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return 0;
            }

            updateDossierList();

            return a;
        }
    }

    public int addVitality(Afdeling afdeling) {
        Connection connection = manager.getConnection();
        int a = 0;
        try {
            insert = connection.prepareStatement("INSERT INTO vitality "
                    + "(vitality_slachtoffernaam, vitality_beschrijving, "
                    + "postcode, huisnr)"
                    + "VALUES (?,?,?,?) "
                    + "RETURNING vitality_id;");
            insert.setString(1, afdeling.getVitality_slachtoffernaam());
            insert.setString(2, afdeling.getVitality_beschrijving());
            insert.setString(3, afdeling.getPostcode());
            insert.setString(4, afdeling.getHuisnr());
            ResultSet rs = insert.executeQuery();
            rs.next();
            a = rs.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            try {
                insert.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return 0;
            }
            updateDossierList();
            return a;
        }
    }

    public Forensic getForensic(int id, int resultaat_id) {
        Connection connection = manager.getConnection();
        ResultSet resultSet = null;
        Forensic forensic = null;
        try {
            select = connection.prepareStatement(
                    "SELECT forensic_id, forensic_dossiernr, "
                            + "forensic_onderzoeker_id, forensic_factuurbedrijf,"
                            + "forensic_besparing, forensic_naam "
                            + "FROM forensic WHERE forensic_id = ?");
            select.setInt(1, id);

            resultSet = select.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            if (resultSet != null) {
                while (resultSet.next()) {

                    forensic = new Forensic(resultSet.getInt("forensic_id"),
                            resultSet.getInt("forensic_dossiernr"),
                            resultSet.getInt("forensic_onderzoeker_id"),
                            resultSet.getString("forensic_factuurbedrijf"),
                            resultSet.getString("forensic_besparing"),
                            getResultaat(resultaat_id),
                            resultSet.getString("forensic_naam"));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                select.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return forensic;
    }

    public Resultaat getResultaat(int id) {
        Connection connection = manager.getConnection();
        ResultSet resultSet = null;
        Resultaat resultaat = null;
        try {
            select = connection.prepareStatement(
                    "SELECT resultaat_id, resultaat_beschrijving FROM resultaat WHERE resultaat_id = ?");
            select.setInt(1, id);

            resultSet = select.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            if (resultSet != null) {
                while (resultSet.next()) {
                    resultaat = new Resultaat(resultSet.getInt("resultaat_id"), resultSet.getString("resultaat_beschrijving"));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                select.close();
                connection.close();
            } catch (SQLException e) {
                //
            }
        }
        return resultaat;
    }

    public Status getStatus(int id) {
        Connection connection = manager.getConnection();
        ResultSet resultSet = null;
        Status status = null;
        try {
            select = connection.prepareStatement(
                    "SELECT status_id, status_beschrijving FROM status WHERE status_id = ?");
            select.setInt(1, id);

            resultSet = select.executeQuery();
        } catch (SQLException e) {
            //
        }
        try {
            if (resultSet != null) {
                while (resultSet.next()) {
                    status = new Status(resultSet.getString("status_beschrijving"), resultSet.getInt("status_id"));
                }
            }
        } catch (SQLException e) {
            //
        } finally {
            try {
                select.close();
                connection.close();
            } catch (SQLException e) {
                //
            }
        }
        return status;
    }

    public Persoon getExpert(int id) {
        Connection connection = manager.getConnection();
        ResultSet resultSet = null;
        Persoon expert = null;
        try {
            select = connection.prepareStatement(
                    "SELECT persoon_voornaam, persoon_achternaam, persoon_tussenvoegsel, persoon_email, persoon_telnr FROM persoon WHERE persoon_id = ?");
            select.setInt(1, id);

            resultSet = select.executeQuery();
        } catch (SQLException e) {
            //
        }
        try {
            if (resultSet != null) {
                while (resultSet.next()) {
                    expert = new Persoon(0, resultSet.getString("persoon_voornaam"),
                            resultSet.getString("persoon_achternaam"), resultSet.getString("persoon_tussenvoegsel"), resultSet.getString("persoon_email"),
                            null, resultSet.getString("persoon_telnr"), 0, 1);
                }
            }
        } catch (SQLException e) {
            //
        } finally {
            try {
                select.close();
                connection.close();
            } catch (SQLException e) {
                //
            }
        }
        return expert;
    }

    public Opdrachtgever getOpdrachtgever(int id) {
        Connection connection = manager.getConnection();
        ResultSet resultSet = null;
        Opdrachtgever opdrachtgever = null;
        try {
            select = connection.prepareStatement(
                    "SELECT ogever_id, ogever_naam, ogever_telefoonnr, "
                            + "ogever_contactpersoon, ogever_contactpersoon_telnr FROM opdrachtgever"
                            + " WHERE ogever_id = ?");
            select.setInt(1, id);

            resultSet = select.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            if (resultSet != null) {
                while (resultSet.next()) {
                    opdrachtgever = new Opdrachtgever(resultSet.getInt("ogever_id"),
                            resultSet.getString("ogever_naam"),
                            resultSet.getString("ogever_telefoonnr"),
                            resultSet.getString("ogever_contactpersoon"),
                            resultSet.getString("ogever_contactpersoon_telnr"));
                }
            }
        } catch (SQLException e) {
            //
        } finally {
            try {
                select.close();
                connection.close();
            } catch (SQLException e) {
                //
            }
        }
        return opdrachtgever;
    }

    public Dossier get(int id) {
        for (Dossier d : dossiers) {
            if (d.getId() == id) {
                return d;
            }
        }
        return null;
    }

    public List<Dossier> getAll() {
        return dossiers;
    }

    public List<Status> getStates() {
        return status;
    }
    
    public List<Opdrachtgever> getClients() {
        Connection connection = manager.getConnection();
        opdrachtgevers = new ArrayList<>();
        ResultSet resultSet = null;
        try {
            select = connection.prepareStatement(
                    "SELECT ogever_id, ogever_naam, ogever_telefoonnr, "
                    + "ogever_contactpersoon, ogever_contactpersoon_telnr"
                    + " FROM opdrachtgever;");
            resultSet = select.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            if (resultSet != null) {
                while (resultSet.next()) {
                    opdrachtgevers.add(new Opdrachtgever(
                            resultSet.getInt("ogever_id"),
                            resultSet.getString("ogever_naam"),
                            resultSet.getString("ogever_telefoonnr"),
                            resultSet.getString("ogever_contactpersoon"),
                            resultSet.getString("ogever_contactpersoon_telnr")
                    ));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                select.close();
                connection.close();
            } catch (SQLException e) {
                //
            }
        }
        return opdrachtgevers;
    }
    
    public void archive(int id) {
        Connection connection = manager.getConnection();
        try {
            select = connection.prepareStatement(
                    "UPDATE dossier SET dossier_status_id = '4' WHERE dossier_id = ?;");
            select.setInt(1, id);
            System.out.println("Archive on id: " + id);
            select.executeUpdate();
        } catch (SQLException e) {
        } finally {
            try {
                select.close();
                connection.close();
            } catch (SQLException e) {
                //
            }
        }
        updateDossierList();
    }

    public void recover(int id) {
        Connection connection = manager.getConnection();
        try {
            select = connection.prepareStatement(
                    "UPDATE dossier SET dossier_status_id = 1 WHERE dossier_id = ?;");
            select.setInt(1, id);
            System.out.println("Recover on id: " + id);
            select.executeUpdate();
        } catch (SQLException e) {
        } finally {
            try {
                select.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        updateDossierList();
    }

    private Afdeling getProperty(int id) {
        Connection connection = manager.getConnection();
        ResultSet resultSet = null;
        Afdeling afdeling = null;
        try {
            select = connection.prepareStatement(
                    "SELECT property_id, property_adres, "
                            + "property_plaats, property_huisnr, "
                            + "property_toevoeging, property_type, postcode, "
                            + "naam_verzekerde "
                            + "FROM property WHERE property_id = ?");
            select.setInt(1, id);

            resultSet = select.executeQuery();
        } catch (SQLException e) {
            //
        }
        try {
            if (resultSet != null) {
                while (resultSet.next()) {
                    afdeling = new Afdeling(
                            resultSet.getInt("property_id"),
                            resultSet.getString("property_adres"),
                            resultSet.getString("property_plaats"),
                            resultSet.getString("property_huisnr"),
                            resultSet.getString("property_toevoeging"),
                            resultSet.getString("property_type"),
                            resultSet.getString("postcode"),
                            resultSet.getString("naam_verzekerde")
                    );
                }
            }
        } catch (SQLException e) {
            //
        } finally {
            try {
                
                select.close();
                connection.close();
            } catch (SQLException e) {
                //
            }
        }
        return afdeling;
    }

    private Afdeling getMobility(int id) {
        Connection connection = manager.getConnection();
        ResultSet resultSet = null;
        Afdeling afdeling = null;
        try {
            select = connection.prepareStatement(
                    "SELECT mobility_id, mobility_kenteken, "
                            + "mobility_automaat, postcode, "
                            + "huisnr, naam_verzekerde "
                            + "FROM mobility WHERE mobility_id = ?");
            select.setInt(1, id);
            resultSet = select.executeQuery();
        } catch (SQLException e) {
            //
        }
        try {
            if (resultSet != null) {
                while (resultSet.next()) {
                    afdeling = new Afdeling(
                            resultSet.getInt("mobility_id"),
                            resultSet.getString("mobility_kenteken"),
                            resultSet.getInt("mobility_automaat"),
                            resultSet.getString("postcode"),
                            resultSet.getString("naam_verzekerde"),
                            resultSet.getString("huisnr") 
                    );
                }
            }
        } catch (SQLException e) {
            //
        } finally {
            try {
                select.close();
                connection.close();
            } catch (SQLException e) {
                //
            }
        }
        return afdeling;
    }

    public void setStatus(int id, int status) {
        Connection connection = manager.getConnection();
        try {
            select = connection.prepareStatement(
                    "UPDATE dossier SET dossier_status_id = ? WHERE dossier_id = ?;");
            select.setInt(1, status);
            select.setInt(2, id);
            System.out.println("Setting status to: " + status + " on id: " + id);
            select.executeUpdate();
        } catch (SQLException e) {

        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private Afdeling getVitality(int id) {
        Connection connection = manager.getConnection();
        ResultSet resultSet = null;
        Afdeling afdeling = null;
        try {
            select = connection.prepareStatement(
                    "SELECT vitality_id, vitality_slachtoffernaam, "
                            + "vitality_beschrijving, postcode, huisnr "
                            + "FROM vitality WHERE vitality_id = ?");
            select.setInt(1, id);

            resultSet = select.executeQuery();
        } catch (SQLException e) {
            //
        }
        try {
            if (resultSet != null) {
                while (resultSet.next()) {
                    afdeling = new Afdeling(
                            resultSet.getInt("vitality_id"),
                            resultSet.getString("vitality_slachtoffernaam"),
                            resultSet.getString("vitality_beschrijving"),
                            resultSet.getString("postcode"),
                            resultSet.getString("huisnr")
                    );

                }
            }
        } catch (SQLException e) {
            //
        } finally {
            try {
                select.close();
                connection.close();
            } catch (SQLException e) {
                //
            }
        }
        return afdeling;
    }


}
