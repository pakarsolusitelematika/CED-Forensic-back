package nl.ced.persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;
import javax.inject.Singleton;
import nl.ced.model.Persoon;
import nl.ced.utility.BCrypt;
import nl.ced.utility.Database;


@Singleton
public class PersoonDAO {

    private Database manager;
    private PreparedStatement select;
    private PreparedStatement insert;
    private PreparedStatement delete;
    private PreparedStatement check;
    private List<Persoon> personen = new ArrayList<>();
    volatile static PersoonDAO s;

    public PersoonDAO() {
        this.manager = new Database();
        updatePersonenList();
    }
    
    
    public void updatePersonenList() { 
        Connection connection = manager.getConnection();
        personen = new ArrayList<>();

        ResultSet resultSet = null;
        try {
            select = connection.prepareStatement(
                    "SELECT persoon_id,persoon_voornaam,persoon_achternaam,persoon_tussenvoegsel,"
                            + "persoon_email, persoon_wachtwoord, persoon_telnr, persoon_rol_id, persoon_active"
                            + " FROM persoon;");
            resultSet = select.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        } try {
            if (resultSet != null) {
                while (resultSet.next()) {
                    personen.add(new Persoon(
                            resultSet.getInt("persoon_id"), 
                            resultSet.getString("persoon_voornaam"), 
                            resultSet.getString("persoon_achternaam"), 
                            resultSet.getString("persoon_tussenvoegsel"),
                            resultSet.getString("persoon_email"), 
                            resultSet.getString("persoon_wachtwoord"),
                            resultSet.getString("persoon_telnr"),
                            resultSet.getInt("persoon_rol_id"),
                            resultSet.getInt("persoon_active")));
                } 
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                select.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public List<Persoon> getAll() {
        Connection connection = manager.getConnection();
        personen = new ArrayList<>();

        ResultSet resultSet = null;
        try {
            select = connection.prepareStatement(
                    "SELECT persoon_id,persoon_voornaam,persoon_achternaam,persoon_tussenvoegsel,"
                            + "persoon_email, persoon_wachtwoord, persoon_telnr, persoon_rol_id, persoon_active"
                            + " FROM persoon;");
            resultSet = select.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        } try {
            if (resultSet != null) {
                while (resultSet.next()) {
                    personen.add(new Persoon(
                            resultSet.getInt("persoon_id"), 
                            resultSet.getString("persoon_voornaam"), 
                            resultSet.getString("persoon_achternaam"), 
                            resultSet.getString("persoon_tussenvoegsel"),
                            resultSet.getString("persoon_email"), 
                            resultSet.getString("persoon_wachtwoord"),
                            resultSet.getString("persoon_telnr"),
                            resultSet.getInt("persoon_rol_id"),
                            resultSet.getInt("persoon_active"))); // Persoon.active bestaat niet in db.
                  
                } 
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                select.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return personen;
    }

    public Persoon get(int id) {
        Persoon idPersoon = null;

        for(Persoon p:getAll()){
            if(p.getId() == id){
                idPersoon = p;
            }
        }
        return idPersoon;
    }

    public Persoon getByEmail2(String email) {
        System.out.println("PERSOONDAO.getByEmail -> email arg passed = " + email);
        Persoon persoon = null;
        System.out.println("BETCHA FUCKING ASS IT WONT BE ABLE TO FIND THIS FUCKIGN USER");
        for(Persoon u : personen) {
            System.out.println(u.getEmail());
        }
        Optional<Persoon> result = personen.stream()
                .filter(user -> user.getEmail().equals(email))
                .findAny();
        return result.isPresent()
                ? result.get()
                : null;
    }
    
    public Persoon getByEmail(String email) {
        Connection connection = manager.getConnection();
        ResultSet rs;
        Persoon persoon = new Persoon();
        try {
            PreparedStatement getAll = connection.prepareStatement("SELECT * FROM persoon WHERE persoon_email = ? ");
            getAll.setString(1, email);
            rs = getAll.executeQuery();
            while (rs.next()){
                persoon.setId(rs.getInt("persoon_id"));
                persoon.setVoornaam(rs.getString("persoon_voornaam"));
                persoon.setAchternaam(rs.getString("persoon_achternaam"));
                persoon.setEmail(rs.getString("persoon_email"));
                persoon.setWachtwoord(rs.getString("persoon_wachtwoord"));
                persoon.setRol(rs.getInt("persoon_rol_id"));
                
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                connection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return persoon;
    }
    public int createPersoon(String voornaam, String achternaam, String
            tussenvoegsel, String email, String wachtwoord, String
            telnr, int rol_id) {
        Connection connection = manager.getConnection();
        
        int affectedRows;
        int generatedId = 0;
        try {
            insert = connection.prepareStatement("INSERT INTO persoon (persoon_voornaam," +
                    "persoon_achternaam,persoon_tussenvoegsel,persoon_email,"
                    + "persoon_wachtwoord,persoon_telnr,persoon_rol_id,persoon_active)"
                    + "VALUES (?,?,?,?,?,?,?,?);", Statement.RETURN_GENERATED_KEYS);
            insert.setString(1, voornaam);
            insert.setString(2, achternaam);
            insert.setString(3, tussenvoegsel);
            insert.setString(4, email);
            insert.setString(5, BCrypt.hashpw(wachtwoord, BCrypt.gensalt()));
            insert.setString(6, telnr);
            insert.setInt(7, rol_id);
            insert.setInt(8, 1);

            affectedRows = insert.executeUpdate();
            
            if (affectedRows == 0) {
                throw new SQLException("No rows affected");
            } 
            System.out.println("Generated keys:" + insert.getGeneratedKeys());
            try (ResultSet generatedKeys = insert.getGeneratedKeys()) {
                if(generatedKeys.next()){
                    System.out.println("in: " + generatedKeys.getInt(1));
                    generatedId = generatedKeys.getInt(1);
                }else{
                    throw new SQLException("No ID was obtained.");
                }
            }
        
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                insert.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
                
            }
        }
        
        updatePersonenList();
        return generatedId;
        
    }
    
    public void updatePersoon(int id, String voornaam, String achternaam, String tussenvoegsel,
            String email, String wachtwoord, String telnr, int rol_id) {
        Connection connection = manager.getConnection();
        try {
                select = connection.prepareStatement(
                        "UPDATE persoon SET persoon_voornaam = ?, persoon_achternaam = ?, persoon_tussenvoegsel = ?, persoon_email = ?,persoon_wachtwoord = ?,persoon_telnr = ?, persoon_rol_id = ? WHERE persoon_id = ?;");
                select.setString(1, voornaam);
                select.setString(2, achternaam);
                select.setString(3, tussenvoegsel);
                select.setString(4, email);
                select.setString(5, BCrypt.hashpw(wachtwoord, BCrypt.gensalt()));
                select.setString(6, telnr);
                select.setInt(7, rol_id);
                select.setInt(8, id);
                int response = select.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                select.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        
        updatePersonenList();
                
    }
    
    public void removePersoon(int id) {
        Connection connection = manager.getConnection();   
        try {
            select = connection.prepareStatement(
                    "UPDATE persoon SET persoon_active = 1 WHERE persoon_id = ?;");
            select.setInt(1, id);
            select.executeUpdate();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }finally {
            try {
                connection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        
        updatePersonenList();

    }

   
    public void unremovePersoon(int id) {
        Connection connection = manager.getConnection();
        try {
            select = connection.prepareStatement(
                    "UPDATE persoon SET persoon_active = 0 WHERE persoon_id = ?;");
            select.setInt(1, id);
            select.executeUpdate();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }finally {
            try {
                connection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        
        updatePersonenList();
    }


        public static PersoonDAO getInstance() throws SQLException {
        if (s == null) {
            synchronized (PersoonDAO.class) {
                if (s == null) {
                    s = new PersoonDAO();
                }
            }
        }
        return s;
    }
}
