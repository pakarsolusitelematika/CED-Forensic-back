package nl.ced.resource;

import com.fasterxml.jackson.annotation.JsonView;
import com.google.inject.Singleton;
import io.dropwizard.auth.Auth;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import nl.ced.View;
import nl.ced.model.Afdeling;
import nl.ced.model.Dossier;
import nl.ced.model.Opdrachtgever;
import nl.ced.model.Status;
import nl.ced.service.FileService;

/**
 * Meer informatie over resources:
 * https://jersey.java.net/documentation/latest/user-guide.html#jaxrs-resources
 *
 * @author Peter van Vliet
 */
@Singleton
@Path("/files")
@Produces(MediaType.APPLICATION_JSON)
public class FileResource {

    private final FileService service;

    @Inject
    public FileResource(FileService service) {

        this.service = service;   
    }
    
    @GET
    @JsonView(View.Public.class)
    @RolesAllowed({"1", "2"})
    public Collection<Dossier> retrieveAll() {
        return service.getAll();
    }
    
    @GET
    @Path("/states")
    @JsonView(View.Public.class)
    @RolesAllowed({"1", "2"})
    public Collection<Status> retrieveStates() {
        return service.getStatus();
    }
    @GET
    @Path("/clients")
    @JsonView(View.Public.class)
    @RolesAllowed({"1", "2"})
    public Collection<Opdrachtgever> retrieveClients() {
        return service.getClients();
    }
    
    
    @GET
    @Path("/{id}")
    @JsonView(View.Public.class)
    @RolesAllowed({"1", "2"})
    public Dossier retrieve(@PathParam("id") int id){
        return service.get(id);
    }

    @DELETE
    @Path("/{id}")
    @JsonView(View.Public.class)
    @RolesAllowed({"1", "2"})
    public void archive(@PathParam("id") int id){ service.archive(id); }

    @POST
    @Path("/{id}")
    @JsonView(View.Public.class)
    @RolesAllowed({"1", "2"})
    public void recover(@PathParam("id") int id){
        System.out.println("Recover in FileResource");
        service.recover(id);
    }

    @POST
    @Path("/{id}")
    @JsonView(View.Public.class)
    @RolesAllowed({"1", "2"})
    public void setStatus(@PathParam("id") int id, int statusId){
        System.out.println("setStatus in FileResource");
        service.setStatus(id, statusId);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/clientadd")
    @JsonView(View.Public.class)
    @RolesAllowed({"1", "2"})
    public void createClient(Opdrachtgever opdrachtgever){
        System.out.println("FileResource createClient");
        service.createClient(opdrachtgever);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"1", "2"})
    @Path("/property")
    @JsonView(View.Protected.class)
    //public void create(Dossier dossier) {
    public int createProperty(Afdeling afdeling) {
        return service.addProperty(afdeling);
    }
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"1", "2"})
    @Path("/mobility")
    @JsonView(View.Protected.class)
    //public void create(Dossier dossier) {
    public int createMobility(Afdeling afdeling) {
        return service.addMobility(afdeling);
    }
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"1", "2"})
    @Path("/vitality")
    @JsonView(View.Protected.class)
    //public void create(Dossier dossier) {
    public int createVitality(Afdeling afdeling) {
        return service.addVitality(afdeling);
    }
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"1", "2"})
    @JsonView(View.Protected.class)
    //public void create(Dossier dossier) {
    public int create(Dossier dossier) {
        System.out.println("afdeling_id " + dossier.getAfdelingObjectId());
        System.out.println("afdeling_type " + dossier.getAfdelingObjectType());
        System.out.println("status " + dossier.getStatusObjectId());
        System.out.println("opdrachtgever " + dossier.getOpdrachtgeverObjectId());
        System.out.println("expert " + dossier.getExpertObjectId());
        System.out.println("nummer " + dossier.getNummer());
        System.out.println("Scade_nummer " + dossier.getSchadenummer());
        System.out.println("opdrachtdatum " + dossier.getOpdrachtdatum());
        System.out.println("polisnummer " + dossier.getPolisnummer());
        System.out.println("kwalificatie " + dossier.getKwalificatie());
        System.out.println("Nieuw_datum " + dossier.getNieuw_registratiedatum());
        System.out.println("Stratiedatum " + dossier.getStratiedatum());
        return(service.add(dossier));

    }
}
