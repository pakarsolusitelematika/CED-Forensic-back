package nl.ced.resource;
 
import com.fasterxml.jackson.annotation.JsonView;
import com.google.inject.Singleton;
import io.dropwizard.auth.Auth;
import java.util.Collection;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import nl.ced.View;
import nl.ced.model.Persoon;
import nl.ced.service.UserService;

/**
 * Meer informatie over resources:
 * https://jersey.java.net/documentation/latest/user-guide.html#jaxrs-resources
 *
 * @author Peter van Vliet
 */
@Singleton
@Path("/users")
@Produces(MediaType.APPLICATION_JSON)
public class UserResource {

    private final UserService service;
    //private final ProfielService service;

    @Inject
    public UserResource(UserService service) {
        this.service = service;   
    }
    // DONE
    @GET
    @JsonView(View.Public.class)
    @RolesAllowed({"1", "2"})
    public Collection<Persoon> retrieveAll() {
        return service.getAll();
    }

    // =======================================================
    // ====================  enkel persoon ===================
    
    // DONE
    @GET
    @Path("/{id}")
    @JsonView(View.Public.class)
    @RolesAllowed({"1", "2"})
    public Persoon retrieve(@PathParam("id") int id) {
        return service.get(id);
    }
    // DONE
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"1", "2"})
    @JsonView(View.Protected.class)
    public int create(Persoon user) {
        return service.add(user);
    }
    
    // DONE
    @POST
    @Path("/update")
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"1", "2"})
    @JsonView(View.Protected.class)
    public void update(Persoon user) {
        service.updateInfo(user);
    }

    // DONE
    @GET
    @Path("/me")
    @RolesAllowed({"1", "2"})
    @JsonView(View.Private.class)
    public Persoon authenticate(@Auth Persoon authenticator) {
        return authenticator;
    }
    
    // DONE
    @DELETE
    @Path("/{id}")
    @RolesAllowed({"1", "2"})
    public void delete(@PathParam("id") int id) {
        service.delete(id);
    }

    // DONE
    @POST
    @Path("/unremove/{id}")
    @RolesAllowed({"1", "2"})
    public void unremove(@PathParam("id") int id) {
        service.unremove(id);
    }
}
